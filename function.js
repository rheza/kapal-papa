function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}


function initialize() {
        var latitude = -5.917011,
            longitude = 106.101965,
            radius = 8000, //how is this set up
            center = new google.maps.LatLng(latitude,longitude),
            bounds = new google.maps.Circle({center: center, radius: radius}).getBounds(),
            mapOptions = {
                center: center,
                zoom: 6,
                mapTypeId: google.maps.MapTypeId.HYBRID,
                scrollwheel: true
            };

        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        setMarkers(center, radius, map);
    }

    function setMarkers(center, radius, map) {
        var json = (function () { 
            var json = null; 
            $.ajax({ 
                'async': false, 
                'global': false, 
                'url': "data.json", 
                'dataType': "json", 
                'success': function (data) {
                     json = data; 
                 }
            });
            return json;
        })();

        var circle = new google.maps.Circle({
                strokeColor: '#000000',
                strokeOpacity: 0.0,
                strokeWeight: 0.0,
                fillColor: '#ffffff',
                fillOpacity: 0.0,
                clickable: false,
                map: map,
                center: center,
                radius: radius
            });
        var bounds = circle.getBounds();


        //loop between each of the json elements
        for (var i = 0, length = json.length; i < length; i++) {
            var data = json[i],
            latLng = new google.maps.LatLng(data.lat, data.lng); 

            console.log("JSON + Length = "+ json.length);
           
            
                // Creating a marker and putting it on the map
                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    title: data.content
                });
                infoBox(map, marker, data);
                 console.log("Marker Added: "+ marker.position );
            
            map.panTo(latLng);
        }

        

        

        //circle.bindTo('center', marker, 'position');
    }

    function infoBox(map, marker, data) {
        var contentString ='<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<b>'+toTitleCase(data.content)+'</b>'+
      '<div id="bodyContent">'+
      '<p>Terakhir Bergerak: '+data.hereSince+'</p>'+
      '</div>'+
      '</div>';
        var infoWindow = new google.maps.InfoWindow({
            content: contentString
        });

        // Attaching a click event to the current marker
        google.maps.event.addListener(marker, "click", function(e) {
            //infoWindow.setContent(data.content);
            infoWindow.open(map, marker);
        });

        // Creating a closure to retain the correct data 
        // Note how I pass the current data in the loop into the closure (marker, data)
        (function(marker, data) {
          // Attaching a click event to the current marker
          google.maps.event.addListener(marker, "click", function(e) {
            //infoWindow.setContent(data.content);
            infoWindow.open(map, marker);
          });
        })(marker, data);

        function myClick() {
          setTimeout(
            function() {
              map.setZoom(13);
              
              infoWindow.open(map, marker);
              map.panTo(marker.position);
              console.log("My Click Called");
            }, 2000);
        }

        myClick();

    }

   google.maps.event.addDomListener(window, 'load', initialize);
